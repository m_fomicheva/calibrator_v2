import numpy as np

from collections import Counter


label_map = {
    0: 1,
    1: 0,
    2: 0,
    3: 0,
}


def bergamot_to_binary(labels, verbose=False):
    output = [label_map[lab] for lab in labels]
    if verbose:
        print(Counter(labels).most_common())
        print(Counter(output).most_common())
    return output


def read_values_from_file(path):
    vals = []
    for line in open(path):
        vals.extend([float(v) for v in line.split()])
    return np.asarray(vals)
