import argparse
import math

from collections import defaultdict

import numpy as np

from sklearn.utils import column_or_1d
from matplotlib import pyplot
from scipy.stats import pearsonr

from util import bergamot_to_binary, read_values_from_file


def load_data(confidence_path, labels_path, log_probas=False, map_labels=False, verbose=False):
    confidence_scores = read_values_from_file(confidence_path)
    if log_probas:
        confidence_scores = np.exp(confidence_scores)
    true_labels = read_values_from_file(labels_path)
    if map_labels:
        true_labels = np.asarray(bergamot_to_binary(true_labels, verbose=verbose))
    assert confidence_scores.shape == true_labels.shape
    return confidence_scores, true_labels


def bucket_data_adaptive(confidence_scores, true_labels, bin_size=100, verbose=False, **kwargs):  # TODO: vectorize
    pairs = [(y_hat, y) for y_hat, y in zip(confidence_scores, true_labels)]
    pairs = sorted(pairs, key=lambda x: x[0])
    bin_labels = []
    for i, pair in enumerate(pairs, start=1):
        bin_labels.append(math.floor((i-1)/bin_size) + 1.)
    bins = defaultdict(list)
    for i, pair in enumerate(pairs):
        bins[bin_labels[i]].append(pair)
    bucket_confidence = []
    bucket_accuracy = []
    bucket_size = []
    for _, bin_values in sorted(bins.items(), key=lambda x: x[0]):
        bucket_confidence.append(np.mean([v[0] for v in bin_values]))
        bucket_accuracy.append(np.mean([v[1] for v in bin_values]))
        bucket_size.append(len(bin_values)/len(pairs))
        if verbose:
            print(len(bin_values))
    return bucket_accuracy, bucket_confidence, bucket_size


def bucket_data(confidence_scores, true_labels, n_bins=10, verbose=False, **kwargs):

    """ Reimplementation of sklearn calibration curve,
    as we need the proportions of points in each bin for
    computing the ECE
    """

    true_labels = column_or_1d(true_labels)
    confidence_scores = column_or_1d(confidence_scores)
    bins = np.linspace(0., 1. + 1e-8, n_bins + 1)
    binids = np.digitize(confidence_scores, bins) - 1

    bin_sums = np.bincount(binids, weights=confidence_scores, minlength=len(bins))
    bin_true = np.bincount(binids, weights=true_labels, minlength=len(bins))
    bin_total = np.bincount(binids, minlength=len(bins))

    nonzero = bin_total != 0
    bucket_accuracy = (bin_true[nonzero] / bin_total[nonzero])
    bucket_confidence = (bin_sums[nonzero] / bin_total[nonzero])
    bucket_size = bin_total[nonzero] / np.sum(bin_total[nonzero])

    if verbose:
        print(bin_total[nonzero])

    assert len(bucket_accuracy) == len(bucket_confidence) == len(bucket_size)
    return bucket_accuracy, bucket_confidence, bucket_size


def calculate_calibration_error(bucket_accuracy, bucket_confidence, bucket_size, verbose=False):
    ece = 0.
    for acc_i, conf_i, size_i in zip(bucket_accuracy, bucket_confidence, bucket_size):
        ece += np.abs(conf_i - acc_i) * size_i
    if verbose:
        print(pearsonr(bucket_accuracy, bucket_confidence))
    return ece


def main(confidence_scores, true_labels, adaptive_bins=False, verbose=False):
    bucket_fn = bucket_data_adaptive if adaptive_bins else bucket_data
    bucket_accuracy, bucket_confidence, bucket_size = bucket_fn(confidence_scores, true_labels, verbose=verbose)
    ece = calculate_calibration_error(bucket_accuracy, bucket_confidence, bucket_size, verbose=verbose)
    print('ECE: {:.2f}'.format(ece * 100.))
    pyplot.plot([0., 1.], [0., 1.], linestyle='--', color='black')
    pyplot.plot(bucket_confidence, bucket_accuracy, linestyle='--', marker='o', color='blue')
    pyplot.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--confidence_scores')
    parser.add_argument('-l', '--labels', help='This must be either labels for binary classification'
                                               ' or correct/incorrect labels')
    parser.add_argument('--adaptive_bins', action='store_true')
    parser.add_argument('--log_probas', action='store_true')
    parser.add_argument('--map_labels', action='store_true')
    parser.add_argument('--verbose', action='store_true')
    args = parser.parse_args()
    confidence_scores, true_labels = load_data(
        args.confidence_scores, args.labels, log_probas=args.log_probas, map_labels=args.map_labels)
    main(confidence_scores, true_labels, adaptive_bins=args.adaptive_bins, verbose=args.verbose)
